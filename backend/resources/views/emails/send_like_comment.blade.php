<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title></title>
    <style>
        @media only screen{html{min-height:100%;background:#e7eaec}}@media only screen and (max-width: 616px){.small-float-center{margin:0 auto !important;float:none !important;text-align:center !important}.small-text-center{text-align:center !important}}@media only screen and (max-width: 616px){table.body table.container .show-for-large{display:none !important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width: 616px){table.body img{width:auto;height:auto}table.body center{min-width:0 !important}table.body .container{width:95% !important}table.body .columns{height:auto !important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px !important;padding-right:16px !important}table.body .columns .columns{padding-left:0 !important;padding-right:0 !important}table.body .collapse .columns{padding-left:0 !important;padding-right:0 !important}th.small-4{display:inline-block !important;width:33.33333% !important}th.small-5{display:inline-block !important;width:41.66667% !important}th.small-7{display:inline-block !important;width:58.33333% !important}th.small-8{display:inline-block !important;width:66.66667% !important}th.small-12{display:inline-block !important;width:100% !important}.columns th.small-12{display:block !important;width:100% !important}table.menu{width:100% !important}table.menu td,table.menu th{width:auto !important;display:inline-block !important}table.menu.vertical td,table.menu.vertical th{display:block !important}table.menu[align=center]{width:auto !important}}@media screen and (orientation: portrait){table.body .header{padding-left:10px !important;
            background-image:none !important}.logo{width:146px !important;height:42px !important}}@media only screen and (max-width: 616px){table.body .header{height:auto !important;background-position:right;padding-left:10px !important}table.body .header-column{padding-left:10px !important;padding-right:10px !important}.title-header{font-size:24px !important}.title-welcome{width:220px !important}.title-discount,.title-introductory-non-solvable,.title-results-lesson{width:250px !important}.title-advantages,.title-selection{width:280px !important}.title-congratulations-purchase{width:auto !important}}@media only screen and (max-width: 616px){.social-img{width:34px !important;height:32px !important}table.body .wrapper.unsubscribe{width:200px !important;margin:auto !important}.body .footer .columns{padding-left:0 !important;padding-right:0 !important}}@media only screen and (max-width: 616px){table.body .container{width:100% !important}table.body .large-offset-right-1{padding-right:10px !important}.text-email,.wrapper-link,.wrapper-two-column,.wrapper-two-column-right{padding-left:10px !important;padding-right:10px !important}.large-width-text,.middle-width-text,.small-width-text,.smaller-width-text{width:auto !important}.small-button-res{padding-left:15px !important;padding-right:15px !important;font-size:14px !important}}@media only screen and (max-width: 616px){.common .certificate-img{width:90%}}.phrase-column-right .wrapper-text-phrase:after{position:absolute;top:50%;right:-12px;width:0;height:0;margin-top:-10px;border-top:6px solid transparent;border-left:12px solid #ffffff;border-bottom:6px solid transparent;content:' '}.phrase-column-right .wrapper-text-phrase:before{display:none}.wrapper-text-phrase:before{position:absolute;top:50%;left:-12px;width:0;height:0;margin-top:-10px;border-top:6px solid transparent;border-right:12px solid #ffffff;border-bottom:6px solid transparent;content:' '}@media only screen and (max-width: 616px){.weekly.main-container .container{width:100% !important}.weekly .text-email{padding-left:10px !important;padding-right:10px !important}.weekly .phrase-container,.weekly .song-phrase{width:100% !important;padding-left:10px !important;padding-right:10px !important}.weekly .phrase-container,.weekly .small-width-text{width:auto !important}.weekly .small-button-res{padding-left:15px !important;padding-right:15px !important;font-size:14px !important}.weekly .img-header-weekly,.weekly .series-img{width:100% !important;height:auto !important}.weekly .wrapper-title-main th.small-12{padding-top:80px !important}.weekly th.header-column.columns{padding-bottom:0}.weekly .img-blog,.weekly .img-partners,.weekly .img-webinar,.weekly .label-weekly,.weekly .logo-weekly{margin-left:auto !important;margin-right:auto !important}.weekly .wrap-text-blog,.weekly .wrapper-blog-img,.weekly .wrapper-img-partners,.weekly .wrapper-text-partners{display:block !important}.weekly .wrapper-blog-img{padding-left:10px !important}.weekly .wrap-text-blog{padding-top:20px !important;padding-right:20px !important}.weekly .img-blog,.weekly .img-partners{border-radius:4px}.weekly .wrapper-text-partners{width:auto !important;padding-left:20px !important}.weekly .img-phrase-column th{text-align:right}.weekly .phrase-column-right.img-phrase-column th{text-align:left}.weekly .img-phrase{display:inline}.weekly .link-partners{padding-bottom:20px !important}.weekly .custom-space{padding-top:5px !important;padding-bottom:5px !important}.weekly .logo-weekly{width:132px;height:49px}.weekly .description-weekly-text,.weekly .wrapper-text-blog,.weekly .wrapper-webinars-link{width:auto !important}.weekly .title-webinar,.weekly .wrapper-partners-link,.weekly .wrapper-webinars-link{line-height:24px;text-align:center !important;display:block !important}.weekly .main-title-webinar,.weekly .title-partners,.weekly .wrap-webinar-weekly,.weekly .wrapper-calendar-button{padding-left:10px !important;padding-right:10px !important;display:block !important;text-align:center !important}.weekly .content-lessons-on-skype,.weekly .img-lessons-on-skype,.weekly .link-partners{display:block !important}.weekly .img-lessons-on-skype img,.weekly .wrap-content-lesson{margin:auto !important}.weekly .content-lessons-on-skype{padding-left:0 !important}.weekly .img-lessons-on-skype{padding-right:0 !important}.weekly .text-lessons-on-skype,.weekly .title-lessons-on-skype{text-align:center}.weekly .calendar-button{display:inline-block !important}.weekly .video-rule{width:86% !important}.weekly .button-border-webinar{padding-right:15px;padding-left:15px;font-size:14px}.weekly .small-button-res{padding-left:15px !important;padding-right:15px !important;font-size:14px !important}}
    </style>
</head>
<body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important">
<span class="preheader"
      style="background-color:#4fc87a!important;border-radius:4px 4px 0 0;color:#e7eaec;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden">Сериал полностью оправдывает свое название. Это и есть Большой Взрыв! Взрыв юмора и сарказма, смекалки и мозга! «Теория большого взрыва» понравится всем:от любителей сериалов до научных сотрудников. Сфера интересов главных героев настолько разнообразна, что даже самые придирчивые зрители смогут найти для себя что-то родное. Сериал бьет не в бровь, а в глаз, потому что здесь речь идет о людях, типаж которых нам всем знаком — умные странные ребята со смешными привычками и грустными глазами. За таких всегда радуешься, когда все у них в жизни налаживается. Ага, значит, и я так тоже смогу!</span>
<table class="body"
       style="Margin:0;background:#e7eaec;border-collapse:collapse;border-spacing:0;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;width:100%">
    <tr style="padding:0;text-align:left;vertical-align:top">
        <td class="center" align="center" valign="top"
            style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
            <center data-parsed="" style="min-width:600px;width:100%"><span class="preheader float-center"
                                                                            align="center"
                                                                            style="background-color:#4fc87a!important;border-radius:4px 4px 0 0;color:#e7eaec;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;mso-hide:all!important;opacity:0;overflow:hidden;visibility:hidden">Сериал полностью оправдывает свое название. Это и есть Большой Взрыв! Взрыв юмора и сарказма, смекалки и мозга! «Теория большого взрыва» понравится всем:от любителей сериалов до научных сотрудников. Сфера интересов главных героев настолько разнообразна, что даже самые придирчивые зрители смогут найти для себя что-то родное. Сериал бьет не в бровь, а в глаз, потому что здесь речь идет о людях, типаж которых нам всем знаком — умные странные ребята со смешными привычками и грустными глазами. За таких всегда радуешься, когда все у них в жизни налаживается. Ага, значит, и я так тоже смогу!</span>
                <table class="body weekly-body float-center"
                       style="Margin:0;border-collapse:collapse;border-spacing:0;color:#66767c;float:none;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"
                       align="center">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <td class="center" align="center" valign="top"
                            style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                            <center data-parsed="" style="min-width:600px;width:100%">
                                <table align="center" class="container main-container weekly red-blue float-center"
                                       style="Margin:0 auto;background:0 0;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;min-width:320px;padding:0;text-align:center;vertical-align:top;width:600px">
                                    <tbody>
                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                        <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                            <table align="center" class="container logo-container"
                                                   style="Margin:0 auto;background:0 0;background-color:#d44848;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:600px">
                                                <tbody>
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                                        <table class="row"
                                                               style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                            <tbody>
                                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                                    <p
                                                                            class="topic-header text-center"
                                                                            style="Margin:0;Margin-bottom:10px;background-color:#d44848;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:20px;font-weight:700;letter-spacing:4.44px;line-height:54px;margin:0;margin-bottom:0;padding:0;text-align:center;text-transform:uppercase">
                                                                        Comment</p>

                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            @if ($comment && $comment->image_url)
                                                <table style="background-color:rgb(255,255,255);background-image:url({{$comment->image_url}});background-position:center;background-repeat:no-repeat;background-size:contain;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"
                                                       class="wrapper img-header-weekly" align="center">
                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                        <td class="wrapper-inner"
                                                            style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;padding-left:0!important;padding-right:0!important;text-align:left;vertical-align:top;word-wrap:break-word">
                                                            <table align="center" class="container"
                                                                   style="Margin:0 auto;background:0 0;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:600px">
                                                                <tbody>
                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                                                        <table class="row wrapper-title-main"
                                                                               style="border-collapse:collapse;border-spacing:0;display:table;height:300px;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                                            <tbody>
                                                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                <th class="small-12 large-12 columns first last"
                                                                                    valign="middle"
                                                                                    style="Margin:0 auto;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:584px">
                                                                                </th>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            @endif
                                            <table class="wrapper content" align="center"
                                                   style="background:#fff;border-collapse:collapse;border-radius:0 0 4px 4px;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                    <td class="wrapper-inner"
                                                        style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;padding-left:0!important;padding-right:0!important;text-align:left;vertical-align:top;word-wrap:break-word">
                                                        <div class="wrapper-description">
                                                            <a href="http://localhost:8080/tweets/{{$tweet->id}}" >
                                                                    {{$comment->body}}
                                                            </a>
                                                        </div>
                                                        <span style="display:block;padding-top:5px;padding-bottom:5px"></span>
                                                        <!--Phrase-->
                                                        <div class="phrase" style="background-color:#f5f5f5"><p
                                                                    class="topic-header text-center"
                                                                    style="Margin:0;Margin-bottom:10px;background-color:#d44848;color:#fff;font-family:Arial,Helvetica,sans-serif;font-size:20px;font-weight:700;letter-spacing:4.44px;line-height:54px;margin:0;margin-bottom:0;padding:0;text-align:center;text-transform:uppercase">
                                                                Liked</p>
                                                            <span
                                                                    style="display:block;padding-top:10px;padding-bottom:10px"></span>
                                                            <span
                                                                    style="display:block;padding-top:10px;padding-bottom:10px"></span>
                                                            <!--Phrase 1-->
                                                            <table align="center" class="container phrase-container"
                                                                   style="Margin:0 auto;background:0 0;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:600px">
                                                                <tbody>
                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:20px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                                                        <table class="row"
                                                                               style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                                            <tbody>
                                                                            <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                <th class="phrase-column img-phrase-column small-5 large-3 columns first"
                                                                                    valign="middle"
                                                                                    style="Margin:0 auto;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:24px;padding-right:15px;text-align:left;width:134px">
                                                                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                            <th style="Margin:0;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                                                                @if($user->profile_image)
                                                                                                    <img src="{{$user->profile_image}}"
                                                                                                         alt="phrase"
                                                                                                         width="100"
                                                                                                         height="100"
                                                                                                         class="img-phrase float-right"
                                                                                                         style="-ms-interpolation-mode:bicubic;border-radius:50px;clear:both;display:block;float:right;height:100px;max-width:100%;outline:0;text-align:right;text-decoration:none;width:100px">
                                                                                                @else
                                                                                                    <div
                                                                                                            class="img-phrase float-right"
                                                                                                            style="-ms-interpolation-mode:bicubic;
                                                       border-radius:50px;clear:both;
                                                       float:right;
                                                       height:100px;max-width:100%;outline:0;text-align:right;text-decoration:none;width:100px;background:#eee;
                                                       display: flex;
                                                       justify-content: center;
                                                       align-items: center;
                                                       color: #7957d5;">

                                                                                                        <strong>{{ \mb_strtoupper($user->first_name[0].$user->last_name[0]) }}</strong>
                                                                                                    </div>
                                                                                                @endif
                                                                                            </th>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                                <th class="phrase-column small-7 large-8 columns last"
                                                                                    valign="middle"
                                                                                    style="Margin:0 auto;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0 auto;padding:0;padding-bottom:10px;padding-left:8px;padding-right:16px;text-align:left;width:384px">
                                                                                    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                            <th style="Margin:0;color:#66767c;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0;padding:0;text-align:left">
                                                                                                <p class="wrapper-text-phrase"
                                                                                                   style="Margin:0;Margin-bottom:10px;background-color:#fff;border-radius:4px;color:#66767c;display:inline-block;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:400;line-height:20px;margin:0;margin-bottom:0;max-width:330px;padding:0;padding-bottom:15px;padding-left:15px;padding-right:15px;padding-top:15px;position:relative;text-align:left;vertical-align:top">
                                                                                                    {{$user->first_name . ' '. $user->last_name}}<br>
                                                                                                    {{$user->nickname}}
                                                                                                </p></th>
                                                                                        </tr>
                                                                                    </table>
                                                                                </th>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                            <span style="display:block;padding-top:15px;padding-bottom:15px"></span>

                                                            <span style="display:block;padding-top:30px;padding-bottom:30px"></span>
                                                        </div><!--Blog-->
                                                    </td>
                                                </tr>
                                            </table>
                                            <span style="display:block;padding-top:10px;padding-bottom:10px"></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table><!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</div>
</body>
</html>