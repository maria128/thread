<?php

declare(strict_types=1);

namespace App\Action\Tweet;

final class GetTweetByHashRequest
{
    private $hashLink;

    public function __construct(string $hashLink)
    {
        $this->hashLink = $hashLink;
    }

    public function getHash(): string
    {
        return $this->hashLink;
    }
}
