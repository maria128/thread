<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Action\GenerateHashLink;
use App\Exceptions\TweetNotFoundException;
use App\Repository\TweetRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class GetTweetHashLink
{
    private $tweetRepository;
    private $generateHashLink;

    public function __construct(
        TweetRepository $tweetRepository,
        GenerateHashLink $generateHashLink
    )
    {
        $this->tweetRepository = $tweetRepository;
        $this->generateHashLink = $generateHashLink;
    }

    public function execute(GetTweetHashLinkRequest $request): GetTweetHashLinkResponse
    {
        try {
            $tweet = $this->tweetRepository->getById($request->getId());
        } catch (ModelNotFoundException $ex) {
            throw new TweetNotFoundException();
        }

        $tweet->hash_link =  $tweet->hash_link ?? $this->generateHashLink->hash($request->getId());
        $tweet = $this->tweetRepository->save($tweet);

        return new GetTweetHashLinkResponse($tweet);
    }
}