<?php

declare(strict_types=1);

namespace App\Action\Tweet;

use App\Repository\TweetRepository;

final class GetTweetByHashAction
{
    private $repository;

    public function __construct(TweetRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(GetTweetByHashRequest $request): GetTweetByHashResponse
    {
        $tweet = $this->repository->getByHash($request->getHash());

        return new GetTweetByHashResponse($tweet);
    }
}