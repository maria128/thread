<?php

declare(strict_types=1);

namespace App\Action;

use Carbon\Carbon;
use Illuminate\Support\Str;

final class GenerateHashLink
{

    public function hash()
    {
        return Str::random(4) . Carbon::now()->timestamp;
    }
}
