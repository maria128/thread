<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;


class SendMailLikeTweet
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userId;
    public $tweetId;
    public function __construct($userId, $tweetId)
    {
        $this->userId = $userId;
        $this->tweetId = $tweetId;
    }
    public function broadcastOn()
    {
        return [];
    }
}
