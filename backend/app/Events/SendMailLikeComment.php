<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;


class SendMailLikeComment
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $userId;
    public $tweetId;
    public $commentId;
    public function __construct($userId, $tweetId, $commentId)
    {
        $this->userId = $userId;
        $this->tweetId = $tweetId;
        $this->commentId = $commentId;
    }
    public function broadcastOn()
    {
        return [];
    }
}
