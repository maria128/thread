<?php

namespace App\Listeners;


use App\Events\SendMailLikeTweet;
use App\Repository\TweetRepository;
use App\Repository\UserRepository;
use Illuminate\Support\Facades\Mail;

class SendMailFiredLikeTweet
{
    private $tweetRepository;
    private $userRepository;


    public function __construct(
        TweetRepository $tweetRepository,
        UserRepository $userRepository
    )
    {
        $this->tweetRepository = $tweetRepository;
        $this->userRepository = $userRepository;
    }
    public function handle(SendMailLikeTweet $event)
    {
        $userLiked = $this->userRepository->getById($event->userId);
        $tweet = $this->tweetRepository->getById($event->tweetId);
        $userTweet = $this->userRepository->getById($tweet->author_id);

        Mail::send('emails.send_like_tweet',  ['tweet' => $tweet, 'user' => $userLiked],  function($message) use ($userTweet) {
            $message->to($userTweet->email);
            $message->subject('Event Testing');
        });
    }
}
