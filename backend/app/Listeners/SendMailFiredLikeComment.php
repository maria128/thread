<?php

namespace App\Listeners;

use App\Events\SendMailLikeComment;
use App\Repository\CommentRepository;
use App\Repository\TweetRepository;
use App\Repository\UserRepository;
use Illuminate\Support\Facades\Mail;

class SendMailFiredLikeComment
{
    private $tweetRepository;
    private $userRepository;
    private $commentRepository;


    public function __construct(
        TweetRepository $tweetRepository,
        UserRepository $userRepository,
        CommentRepository $commentRepository
    )
    {
        $this->tweetRepository = $tweetRepository;
        $this->userRepository = $userRepository;
        $this->commentRepository = $commentRepository;
    }
    public function handle(SendMailLikeComment $event)
    {
        $userLiked = $this->userRepository->getById($event->userId);
        $tweet = $this->tweetRepository->getById($event->tweetId);
        $userTweet = $this->userRepository->getById($tweet->author_id);
        $comment = $this->commentRepository->getById($event->commentId);

        Mail::send('emails.send_like_comment',  ['tweet' => $tweet, 'user' => $userLiked, 'comment' => $comment],  function($message) use ($userTweet) {
            $message->to($userTweet->email);
            $message->subject('Event Testing');
        });
    }
}
