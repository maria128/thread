import Vue from 'vue';
import Router from 'vue-router';
import Storage from '@/services/Storage';
import api from '@/api/Api';

// async components
const Feed = () => import(/* webpackChunkName: "feed" */ './views/Feed.vue');
const Profile = () => import(/* webpackChunkName: "profile" */ './views/Profile.vue');
const User = () => import(/* webpackChunkName: "user" */ './views/User.vue');
const Tweet = () => import(/* webpackChunkName: "tweet" */ './views/Tweet.vue');

// auth pages using same chunk name
const SignIn = () => import(/* webpackChunkName: "auth" */ './views/SignIn.vue');
const SignUp = () => import(/* webpackChunkName: "auth" */ './views/SignUp.vue');

Vue.use(Router);

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            redirect: '/feed',
        },
        {
            path: '/feed',
            name: 'feed',
            component: Feed,
            meta: { requiresAuth: true },
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
            meta: { requiresAuth: true },
        },
        {
            path: '/users/:id',
            name: 'user-page',
            component: User,
            meta: { requiresAuth: true },
        },
        {
            path: '/tweets/:id',
            name: 'tweet-page',
            component: Tweet,
            meta: { requiresAuth: true },
        },
        {
            path: '/auth/sign-in',
            name: 'auth.signIn',
            component: SignIn,
            meta: { handleAuth: true },
        },
        {
            path: '/auth/sign-up',
            name: 'auth.signUp',
            component: SignUp,
            meta: { handleAuth: true },
        },
        {
            path: '/:hash(\\w+)',
            meta: { hashLink: true },
        },

    ],
    scrollBehavior: () => ({ x: 0, y: 0 }),
});

function hash(param) {
    return api.get(`/tweets/hash/${param}`);
}

// check auth routes
router.beforeEach(
    async (to, from, next) => {
        const isAuthenticatedRoute = to.matched.some(record => record.meta.requiresAuth);
        const isAuthSectionRoute = to.matched.some(record => record.meta.handleAuth);
        const isHashLink = to.matched.some(record => record.meta.hashLink);
        if (isAuthenticatedRoute && !Storage.hasToken()) {
            next({ name: 'auth.signIn' });
            return;
        }

        if (isAuthSectionRoute && Storage.hasToken()) {
            next({ path: '/' });
            return;
        }

        if (isHashLink) {
            const param = to.params.hash;
            console.log(param);
            const idTweet = await hash(param);
            console.log(idTweet.id);
            if (idTweet) {
                next({ path: `/tweets/${idTweet.id}` });
            } else {
                next();
            }
            return;
        }
        next({ path: to });
    },
);

export default router;
